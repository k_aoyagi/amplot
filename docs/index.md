# amplot document

## 概要

### コロプレス図

下記形式のデータから、地域ごとに色分けされた地図を作成できます。

region  |title1  |title2
--------|--------|--------
Hokkaido|     1.1|    2.1
Aomori  |     2.0|    0.3
...     |     ...|    ...


## 使い方

### インストール及びプロジェクト作成

1. [R](https://cran.r-project.org/bin/windows/base/)及び [RStudio](https://www.rstudio.com/products/rstudio/download/#download) をダウンロード・インストールします。
1. RStudioのConsoleに以下のコマンドを入力し、amplotをインストールします。※インターネット接続が要ります。
  ```r
  install.packages("devtools"); devtools::install_gitlab("k_aoyagi/amplot")
  ```
1. File > New Project を選択
  ![](2_create_new_project.png)
1. New Directory を選び、下の方にある amplot projectを選択
  ![](3_amplot_project.png)
1. 新しいDirectory Nameを入力し、Create projectを選択
  ![](4_create.png) 
1. main.Rのファイルが開きます。そのまま全体を実行するとテスト用の色分け地図をoutput_exampleフォルダに出力するようになっています。コメントでサンプルを記載しているので、必要なもののみをコメントアウトしてファイル名等を修正して実行してください。各関数の詳細な説明については、下記のリファレンスを参照してください

### リファレンス
[リファレンス](reference.md)
