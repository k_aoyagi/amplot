# リファレンス

## single_choropleth_jp

```r
single_choropleth_jp(
  input_df,
  title = input_df %>% select(-1) %>% colnames(),
  legend_label = "",
  map_df = get_jpn_data_okinawa_moved(),
  palette = get_color_palette() %>% mutate_color(~ .x / 2),
  layout = theme(),
  .layout = get_layout_default_jp() + layout,
  step = 5,
  nsmall = 3,
  scale = partial(get_step_scale, .step = step, .nsmall = nsmall),
  line = get_okinawa_line()
)
```

### 概要

single_choropleth_jpはデータフレームやtibbleを受け取り、ggplot2形式の色分け地図のリストを返します。

### 引数

#### `input_df`

input_dfに以下フォーマットのcsvを読み込むと、カラム分の色分け地図のリストを出力します。
* 一番上の行はカラム名
* 1列目に、使用するmap_data(日本の場合は7都道府県(アルファベット表記))と同内容のregionカラムが含まれる
* 2列目以降はregionに対応するそのカラムの分類の数値で、カラム名にグラフのタイトルを入れます。
	
region  |title1  |title2
--------|--------|--------
Hokkaido|     1.1|    2.1
Aomori  |     2.0|    0.3
...     |     ...|    ...


#### `title`
  タイトルにはデフォルトで`input_df`のカラム名が使用されますが、この引数で指定することもできます。単一の文字列又はカラム数分のVectorを入れます

#### `legend_label`
  判例のラベルです。デフォルトだと無しになります。

#### `map_df`
  マップデータです。ggplotのmap_dataを入れます。デフォルトでは沖縄を移動した日本のマップが使用されます

#### `palette`
  色分け地図のメインカラーを指定します。ここで指定した順でマップの色が決まります。デフォルトでは12色=12マップ分あります。デフォルトのpaletteは以下の通りです。
```r
color_palette = c(
  "red","green","blue",
  "yellow","magenta","cyan",
  "orange","springgreen","violet",
  "chartreuse", "azure", "#FF00CC"
) %>% mutate_color(~ .x / 2)
```

#### `layout`
  マップの見た目を指定するthemeを追加できます。 [ggplot2 theme](https://ggplot2.tidyverse.org/reference/theme.html) を参照してください。

#### `.layout`
  デフォルトのthemeを完全に上書きする場合はこちらの`.layout`を使用します。デフォルトのthemeは以下の通りです。
```r
theme(
  plot.title = element_text(hjust = 0.5),
  axis.title = element_blank(),
  axis.text = element_blank(),
  axis.ticks = element_blank(),
  panel.grid = element_blank(),
  panel.background = element_blank(),
  legend.position = c(1, 0),
  legend.justification = c(1.1, -.5),
  legend.key.width = unit(.5, "cm"),
  legend.key.height = unit(1.2, "cm"),
  aspect.ratio = 1.3
)
```
#### `step`
  色分けの段数を指定します。デフォルトは5です
  
#### `nsmall`
  判例の数値の小数点以下の桁数を指定します。デフォルトは3です。

#### `scale`
  色分けを行うための関数を設定します。関数は、引数にcolorを取りggplotのscale_fill系のオブジェクトを返す必要があります。デフォルトの関数は以下のようになっています。
```r
step_ = 5
nsmall_ = 3
get_step_scale <- function(.color) 
  scale_fill_steps(
    low = "white",
    high = .color,
    n.breaks = step_ - 1,
    show.limits = TRUE,
    nice.breaks = FALSE,
    labels = ~ round(.x, nsmall_) %>% format(nsmall = nsmall_)
  )
```

### 例
```r
# csvを読み込んで色分け地図を作成し"output_examle"フォルダに出力する場合、以下のようにします。
read_csv_ml("example_data.csv") %>%
  single_choropleth_jp(legend_label="DDD") %>%
  save_plots(dir_name="output_example/")

# save_plotに何も指定しなければ"output_plots_(日付)/"フォルダに出力されます
read_csv_ml("example_data.csv") %>%
  single_choropleth_jp(legend_label="DDD") %>%
  save_plots()

# csvでなくエクセル形式を読み込む場合は以下のようにします。
readxl::read_excel("example_data.xlsx") %>%
  single_choropleth_jp(legend_label="DDD") %>%
  save_plots()

# 読み込んだファイルを加工してから色分け地図を作成する事もできます
read_csv_ml("example_data.csv") %>%
  mutate(across(-region, ~round(.x,2))) %>%
  single_choropleth_jp(legend_label="DDD") %>%
  save_plots()

# 色分け地図を作成したあとにggplotを使ってさらに加工するには、以下のようにします。
read_csv_ml("example_data.csv") %>%
  single_choropleth_jp(legend_label="DDD") %>%
  map(~ .x + geom_path( aex(x,y), tibble(x=c(10,20),y=c(40,50))) ) %>%
  save_plots()

# 色やテーマなどを変更するには以下のようにします。
read_csv_ml("example_data.csv") %>%
  single_choropleth_jp(
    legend_label="DDD",
    title = "my plot",
    palette = c("red","green","blue","purple", "brown", "yellow"),
    layout = theme(panel.border = element_rect(linetype = "dashed")),
    step = 4,
    nsmal = 2
  ) %>%
  save_plots()
```

## save_plots

```r
save_plots(
  plots,
  dir_prefix = "output_plots",
  dir_name = paste0(dir_prefix, "_", Sys.Date(), "/"),
  filetype = "svg", ext = filetype,
  ...
)
```

### 概要

ggplotのリストから、複数の画像ファイルを出力する関数です。

### 引数

#### `plots`

ggplotのリスト

#### `dir_prefix`

画像の出力先ディレクトリ名の日付より前の部分です。デフォルトは`output_plots`

#### `dir_name`

画像の出力先ディレクトリ名ですデフォルトはdir_prefix_日付/です

#### `filetype`

出力画像のファイル形式です。デフォルトではsvgです。

#### `...`

[ggsave](https://ggplot2.tidyverse.org/reference/ggsave.html)の任意のオプションを指定できます。 



## その他の関数

その他の関数については今後記載していきます。
